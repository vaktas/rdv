all: generate-backend generate-front

generate-backend:
	go install github.com/deepmap/oapi-codegen/cmd/oapi-codegen@latest
	oapi-codegen -generate chi-server -o backend/api/api.go -package api openapi/spec.yml
	oapi-codegen -generate types -o backend/api/api_types.go -package api openapi/spec.yml

generate-front:
	java -jar openapi/openapi-generator-cli-5.3.0.jar generate -i openapi/spec.yml -g typescript-angular --additional-properties=queryParamObjectFormat=key --additional-properties=withInterfaces=true -o frontend/src/app/api
