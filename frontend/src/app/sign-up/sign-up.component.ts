import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { sha512 } from 'js-sha512';
import Swal from 'sweetalert2';
import { Configuration, UserService } from '../api';
import { SessionService } from '../session.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent {
  public signUpForm: FormGroup

  constructor(
    private router: Router,
    private userService: UserService,
    private fb: FormBuilder,
    private session: SessionService,
    private configAPI: Configuration
  ) {
    this.signUpForm = this.fb.group({
      surname: ['', Validators.required],
      name: ['', Validators.required],
      tel: ['', [Validators.required, Validators.pattern("^0([1-9]([0-9]{2}){3}|[1-9]\.([0-9]{2}\.){3})[0-9]{2}$")]],
      password: ['', Validators.required]
    });
  }

  public onSubmit(): void {
    const v = this.signUpForm.getRawValue();
    this.userService.register(v["surname"], v["name"], v["tel"], sha512(v["password"]))
      .subscribe(user => {
        if (user != null) {
          this.userService.login(user.login, sha512(v["password"]))
            .subscribe((response) => {
              this.session.setUserSession(response.token, response.role, response.uuid);
              this.configAPI.accessToken = this.session.getUserToken();
              Swal.fire({
                title: 'Enregistrement réussi',
                text: 'Votre identifiant a été généré et est: ' + user.login,
                icon: 'success',
                showConfirmButton: true,
                confirmButtonText: 'Ok'
              }).then(() => {
                if (response.role == "user") { this.router.navigate(['/user', user.uuid]) };
                if (response.role == "admin") { this.router.navigate(["/admin"]) };
              })
            });
        } else {
          Swal.fire({
            title: 'Echec de l\'enregistrement',
            text: 'L\'enregistrement a échoué, nous nous excusons du dérangement',
            icon: 'warning',
            showConfirmButton: true,
            confirmButtonText: 'Ok'
          })
        }
      }, err => {
        console.log(err)
      })
  }
}
