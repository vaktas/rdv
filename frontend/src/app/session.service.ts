import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  constructor() { }

  getUserToken(): string {
    return sessionStorage.getItem('token') || "";
  }
  getUserRole(): string {
    return sessionStorage.getItem('role') || "";
  }
  getUserUUID(): string {
    return sessionStorage.getItem('uuid') || "";
  }
  setUserToken(token?: string): void {
    if (typeof(token) === "string") sessionStorage.setItem('token', token);
  }
  setUserRole(role?: string): void {
    if (typeof(role) === "string") sessionStorage.setItem('role', role);
  }
  setUserUUID(uuid?: string): void {
    if (typeof(uuid) === "string") sessionStorage.setItem('uuid', uuid);
  }
  setUserSession(token?: string, role?: string, uuid?: string): void {
    this.setUserRole(role);
    this.setUserToken(token);
    this.setUserUUID(uuid);
  }
  resetUserSession(): void {
    sessionStorage.clear();
  }
  isAuthenticated(): Observable<boolean> {
    return of(this.getUserToken() != "" && this.getUserRole() != "");
  }
  isAdmin(): boolean {
    return this.getUserRole() === "admin";
  }
}
