import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { User, UserService, MotifService } from 'src/app/api';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  public motifs$ = new Observable<Array<string>>();
  public user$ = new Observable<User>();
  public motif: string = "";
  public from: Date | undefined;
  public to: Date | undefined;

  constructor(
    private motifService: MotifService,
    private route: ActivatedRoute,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.user$ = this.userService.getUserById(params["user-uuid"]);
    })
    this.motifs$ = this.motifService.searchMotif(25, 0);
  }
}
