import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { DetailsComponent } from './details/details.component';
import { SlotsGroupModule } from '../slots-group/slots-group.module';

@NgModule({
  declarations: [
    DetailsComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      {path: ':user-uuid', component: DetailsComponent},
    ]),
    SlotsGroupModule
  ]
})
export class UserModule { }
