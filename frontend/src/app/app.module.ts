import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { ApiModule, BASE_PATH, Configuration, ConfigurationParameters } from './api';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SessionService } from './session.service';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { HttpSessionInterceptor } from './http-session.interceptor';
import { environment } from 'src/environments/environment';

registerLocaleData(localeFr, 'fr')
function load(sessionService: SessionService): Configuration {
  let params: ConfigurationParameters =
  {
    accessToken: sessionService.getUserToken()
  };
  return new Configuration(params)
}
@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SignUpComponent,
    SignInComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ApiModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpSessionInterceptor, multi: true },
    SessionService,
    {
      provide: Configuration,
      useFactory: load,
      deps: [SessionService],
      multi: false
    },
    { provide: BASE_PATH, useValue: environment.BASE_PATH },
    { provide: LOCALE_ID, useValue: 'fr' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
