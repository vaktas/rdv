import { Component } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { UserService } from '../api';
import { SessionService } from '../session.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent {
  public isActive = false;
  constructor(
    public sessionService: SessionService,
    private userService: UserService,
    private router: Router
  ) {}

  logout(): void {
    this.userService.logout()
     .subscribe(() => {
       Swal.fire("Déconnecté").then(() => {
         this.sessionService.resetUserSession();
         this.router.navigate(['/']);
       })
     });
  }
}
