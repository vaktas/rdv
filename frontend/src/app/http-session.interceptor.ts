import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {catchError} from 'rxjs/operators';
import { SessionService } from './session.service';
import { Router } from '@angular/router';
import { Configuration } from './api';


@Injectable()
export class HttpSessionInterceptor implements HttpInterceptor {

  constructor(
    private sessionService: SessionService,
    private router: Router,
    private configAPI: Configuration
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError(response => {
        if (response.status == 401) {
          this.sessionService.resetUserSession();
          this.configAPI.accessToken = undefined;
          this.router.navigate(['/']);
        }
        return throwError(response)
      })
    );
  }
}
