import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { sha512 } from 'js-sha512';
import Swal from 'sweetalert2';
import { Configuration, UserService } from '../api';
import { SessionService } from '../session.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent {
  public signInForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private session: SessionService,
    private router: Router,
    private configAPI: Configuration
  ) {
    this.signInForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  public onSubmit() {
    const v = this.signInForm.getRawValue();
    this.userService.login(v['username'], sha512(v['password']))
      .subscribe((resp) => {
        this.session.setUserSession(resp.token, resp.role, resp.uuid);
        this.configAPI.accessToken = this.session.getUserToken();
        Swal.fire('Authentifié').then(() => {
          this.router.navigate(['/user', resp.uuid]);
        })
      });
  }
}
