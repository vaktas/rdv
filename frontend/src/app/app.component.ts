import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { SessionService } from './session.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'frontend';

  constructor(
    private session: SessionService
  ) { }

  public isAuthenticated(): Observable<boolean> {
    return this.session.isAuthenticated();
  }
}
