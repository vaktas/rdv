import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { MotifService } from 'src/app/api';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-element',
  templateUrl: './element.component.html',
  styleUrls: ['./element.component.scss']
})
export class ElementComponent implements OnInit {
  @Input() motif: string = "";
  @Output() updated: EventEmitter<boolean> = new EventEmitter<boolean>();

  public newMotif: string = "";
  public edition: boolean = false;

  constructor(
    private motifService: MotifService
  ) { }

  ngOnInit(): void {
    this.newMotif = this.motif;
  }

  public update() {
    this.edition = false;
    if (this.newMotif === this.motif || this.motif === "") {
      return;
    }
    this.motifService.putMotif(this.motif, this.newMotif).subscribe(() => {
      this.updated.emit(true);
    }, (err) => console.log(err))
  }

  public delete() {
    Swal.fire({
      title: "Suppression d'un motif",
      html: "<p>Attention la supression d'un motif est définitif et entrainera la suppression des créneaux associés.<br><br> Êtes-vous sûr de vouloir continuer ?</p>",
      icon: "warning",
      showCancelButton: true,
      showConfirmButton: true,
      confirmButtonText: "Continuer",
      cancelButtonText: "Annuler"
    }).then(value => {
      if (value.isConfirmed) {
        this.motifService.deleteMotif(this.motif).subscribe(() => {
          this.updated.emit(true);
        }, err => console.log(err));
      }
    });
  }
}
