import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MotifsComponent } from './motifs.component';
import { RouterModule } from '@angular/router';
import { ElementComponent } from './element/element.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    MotifsComponent,
    ElementComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([{path: '', component: MotifsComponent}]),
    FormsModule
  ]
})
export class MotifsModule { }
