import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MotifService } from 'src/app/api';

@Component({
  selector: 'app-motifs',
  templateUrl: './motifs.component.html'
})
export class MotifsComponent implements OnInit {
  public motifs$: Observable<Array<string>> = new Observable();
  public newMotif: string = "";

  constructor(
    private motifService: MotifService
  ) { }

  ngOnInit(): void {
    this.refresh();
  }

  public refresh() {
    this.motifs$ = this.motifService.searchMotif();
  }

  public create() {
    if (this.newMotif === "") return;
    this.motifService.addMotif(this.newMotif).subscribe(() => {
      this.newMotif = "";
      this.refresh();
    })
  }
}
