import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AppointmentComponent } from './appointment.component';
import { ListComponent } from './list/list.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    AppointmentComponent,
    ListComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      {path: '', component: AppointmentComponent}
    ])
  ]
})
export class AppointmentModule { }
