import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MotifService, SlotsGroup, SlotsService } from 'src/app/api';

@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.component.html',
  styleUrls: ['./appointment.component.scss']
})
export class AppointmentComponent implements OnInit {
  public motifs$ = new Observable<Array<string>>();
  public motif: string = "";
  public from: Date | undefined;
  public to: Date | undefined;
  public slotsGroups$ = new Observable<SlotsGroup[]>();

  constructor(
    private motifService: MotifService,
    private slotsService: SlotsService
  ) { }

  ngOnInit(): void {
    this.refreshSlotsGroup();
    this.motifs$ = this.motifService.searchMotif(25, 0);
  }
  public refreshSlotsGroup() {
    this.slotsGroups$ = this.slotsService.searchSlots(false, 25, 0, undefined, this.motif, (this.from) ? (new Date(this.from)).toISOString() : undefined, (this.to) ? (new Date(this.to)).toISOString() : undefined);
  }
  public printPage() {
    window.print();
  }
}
