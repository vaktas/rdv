import { Component, Input, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { SlotsGroup, User, UserService } from 'src/app/api';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @Input() slotsGroup: SlotsGroup | undefined;
  userList$: Observable<User>[] = [];

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
    if (this.slotsGroup == undefined) return;
    this.slotsGroup.appointments?.forEach(appointment => {
      this.userList$.push(this.userService.getUserById(appointment.user ? appointment.user : ""))
    });
  }

  public getUser$(userUUID?: string): Observable<User> {
    if (userUUID == undefined) return of(<User>{});
    return this.userService.getUserById(userUUID);
  }

  public getDate(s: string): Date {
    const v = new Date(s);
    v.setMinutes(v.getMinutes() + v.getTimezoneOffset());
    return v
  }
}
