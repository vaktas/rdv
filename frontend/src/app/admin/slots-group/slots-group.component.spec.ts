import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SlotsGroupComponent } from './slots-group.component';

describe('SlotsGroupComponent', () => {
  let component: SlotsGroupComponent;
  let fixture: ComponentFixture<SlotsGroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SlotsGroupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SlotsGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
