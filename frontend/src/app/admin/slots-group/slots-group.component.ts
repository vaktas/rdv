import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MotifService } from 'src/app/api';

@Component({
  selector: 'app-slots-group',
  templateUrl: './slots-group.component.html',
  styleUrls: ['./slots-group.component.scss']
})
export class SlotsGroupComponent implements OnInit {
  public motifs$ = new Observable<Array<string>>();
  public motif: string = "";
  public from: Date | undefined;
  public to: Date | undefined;

  constructor(
    private motifService: MotifService
  ) { }

  ngOnInit(): void {
    this.motifs$ = this.motifService.searchMotif(25, 0);
  }
}
