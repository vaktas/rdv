import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SlotsGroupComponent } from './slots-group/slots-group.component';
import { SlotsGroupModule } from '../slots-group/slots-group.module';
import { RouterModule } from '@angular/router';
import { CreateSlotsGroupComponent } from './create-slots-group/create-slots-group.component';



@NgModule({
  declarations: [
    SlotsGroupComponent,
    CreateSlotsGroupComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {path: '', redirectTo: 'slots-group'},
      {path: 'slots-group', children: [
        {path: '', component: SlotsGroupComponent},
        {path: 'appointment', loadChildren: () => import('./appointment/appointment.module').then(m => m.AppointmentModule)}
      ]},
      {path: 'motifs', loadChildren: () => import("./motifs/motifs.module").then(m => m.MotifsModule)}
    ]),
    SlotsGroupModule,
  ]
})
export class AdminModule { }
