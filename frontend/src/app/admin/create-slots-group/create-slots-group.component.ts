import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { SlotsGroup, SlotsService } from 'src/app/api';
import { MotifService } from 'src/app/api/api/motif.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-create-slots-group',
  templateUrl: './create-slots-group.component.html',
  styleUrls: ['./create-slots-group.component.scss']
})
export class CreateSlotsGroupComponent implements OnInit {
  public creationForm: FormGroup;
  public motifs$ = new Observable<Array<string>>();

  constructor(
    private fb: FormBuilder,
    private motifService: MotifService,
    private slotsService: SlotsService 
  ) {
    this.creationForm = this.fb.group({
      slotsGroups: this.fb.array([], Validators.required)
    })
  }

  ngOnInit(): void {
    this.refreshMotifs();
  }

  get slotsGroupsFormArray(): FormArray {
    return this.creationForm.get("slotsGroups") as FormArray;
  }

  private refreshMotifs(): void {
    this.motifs$ = this.motifService.searchMotif(25, 0);
  }

  public addAppointment(): void {
    this.slotsGroupsFormArray.push(this.slotsGroupForm());
  }

  private slotsGroupForm(): FormGroup {
    return this.fb.group({
      motif: ['', Validators.required],
      quantity: ['', [Validators.required, Validators.min(1)]],
      quantityPerSlot: ['', [Validators.required, Validators.min(1)]],
      duration: ['', [Validators.required, Validators.min(1)]],
      day: ['', Validators.required],
      from: ['', Validators.required],
      to: ['', Validators.required]
    });
  }

  public removeAppointment(i: number): void {
    this.slotsGroupsFormArray.removeAt(i);
  }

  public onSubmit(): void {
    let slotsGroup = <SlotsGroup[]>[];
    for (let i = 0; i < this.slotsGroupsFormArray.length; i++) {
      const v = this.slotsGroupsFormArray.at(i);
      const quantity = +v.get("quantity")?.value;
      const quantityPerSlot = +v.get("quantityPerSlot")?.value;
      const duration = +v.get("duration")?.value;
      const from = v.get("day")?.value + "T" + v.get("from")?.value + ":00.000Z";
      const fromDate = new Date(from);
      const to = v.get("day")?.value + "T" + v.get("to")?.value + ":00.000Z";
      const toDate = new Date(to);
      const delta = Math.abs(toDate.getHours() - fromDate.getHours()) * 60 + (toDate.getMinutes() - fromDate.getMinutes());
      const maxSlots = Math.floor(delta/duration);
      if (duration != 0 && maxSlots < quantity) {
        Swal.fire({
          title: 'Création impossible',
          text: 'Sur la plage horaire numéro ' + i + ' il est impossible de mettre ' + quantity + ' créneau(x). Maximum possible : ' + maxSlots,
          icon: 'error'
        })
        return;
      }
      slotsGroup.push(<SlotsGroup>{
        quantityPerSlot: quantityPerSlot,
        quantity: (quantity != 0) ? quantity : maxSlots,
        duration: duration,
        motif: v.get("motif")?.value,
        from: from,
        to: to,
      });
    }

    slotsGroup.forEach(slotsGroup => {
      this.slotsService.addSlots(slotsGroup.motif, slotsGroup.from, slotsGroup.to, slotsGroup.quantity, slotsGroup.quantityPerSlot, slotsGroup.duration)
        .subscribe(() => {
          console.log("slotsGroup created : " + slotsGroup)
        });
    })
    this.slotsGroupsFormArray.clear();
  }

  public takeMaximum(i: number): void {
    const v = this.slotsGroupsFormArray.at(i);
    const duration = +v.get("duration")?.value;
    const from = v.get("day")?.value + "T" + v.get("from")?.value + ":00.000Z";
    const fromDate = new Date(from);
    const to = v.get("day")?.value + "T" + v.get("to")?.value + ":00.000Z";
    const toDate = new Date(to);
    const delta = Math.abs(toDate.getHours() - fromDate.getHours()) * 60 + (toDate.getMinutes() - fromDate.getMinutes());
    const maxSlots = Math.floor(delta/duration);
    this.slotsGroupsFormArray.at(i).get("quantity")?.setValue(maxSlots);
  }
}
