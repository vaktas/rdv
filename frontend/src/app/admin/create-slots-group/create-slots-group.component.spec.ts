import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSlotsGroupComponent } from './create-slots-group.component';

describe('CreateSlotsGroupComponent', () => {
  let component: CreateSlotsGroupComponent;
  let fixture: ComponentFixture<CreateSlotsGroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateSlotsGroupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSlotsGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
