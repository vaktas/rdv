import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Appointment, AppointmentService, User, UserService } from 'src/app/api';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-appointments-details',
  templateUrl: './appointments-details.component.html',
  styleUrls: ['./appointments-details.component.scss']
})
export class AppointmentsDetailsComponent implements OnInit {
  @Input() appointments: Appointment[] = [];
  @Input() slotsGroupUUID: string = "";
  userList$: Observable<User>[] = [];

  constructor(
    private userService: UserService,
    private appointmentService: AppointmentService,
  ) { }

  ngOnInit(): void {
    if (this.slotsGroupUUID == "") {
      throw new Error("slotsGroupUUID need to be assigned");
    }
    if (this.appointments === undefined || this.appointments.length == 0) return
    this.appointments.forEach(appointment => {
      this.userList$.push(this.userService.getUserById(appointment.user ? appointment.user : ""))
    });
  }

  public getDate(s: string): Date {
    const v = new Date(s);
    v.setMinutes(v.getMinutes() + v.getTimezoneOffset());
    return v
  }

  public removeAppointment(userUUID?: string, i?: number): void {
    if (userUUID == undefined) return;
    if (i == undefined) return;
    if (this.appointments[i] == undefined) return

    Swal.fire({
      title: "Suppression d'un rendez-vous",
      text: "Attention êtes-vous sûr de vouloir supprimer ce rendez-vous ?",
      icon: "warning",
      showCancelButton: true,
      showConfirmButton: true
    }).then(value => {
      if (value.isDismissed) {
        console.log("Suppression annulée");
      }
      if (value.isConfirmed) {
        this.appointmentService.unbookSlot(this.slotsGroupUUID, userUUID)
          .subscribe(() => {
            Swal.fire("Rendez-vous supprimé").then(() => {
              this.appointments.splice(i, 1);
            })
          },
            err => {
              Swal.fire({
                title: 'Erreur dans la suppression',
                text: err,
                icon: 'error'
              });
            });
      }
    });
  }

  public checkAppointment(userUUID?: string, i?: number): void {
    if (userUUID == undefined) return;
    if (i == undefined) return;
    if (this.appointments[i] == undefined) return
    this.appointmentService.checkAppointment(this.slotsGroupUUID, userUUID)
      .subscribe(() => {
        Swal.fire("Rendez-vous checké").then(() => {
          this.appointments[i].done = true;
        })
      },
        err => {
          Swal.fire({
            title: 'Erreur dans la suppression',
            text: err,
            icon: 'error'
          });
        });
  }
}
