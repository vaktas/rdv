import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SlotsGroup, UserService } from 'src/app/api';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-search-or-create-user',
  templateUrl: './search-or-create-user.component.html',
  styleUrls: ['./search-or-create-user.component.scss']
})
export class SearchOrCreateUserComponent implements OnInit {
  @Input() slotsGroup?: SlotsGroup;
  @Output() userUUID = new EventEmitter<string>();

  public userForm: FormGroup;
  constructor(
    private userService: UserService,
    private fb: FormBuilder
  ) {
    this.userForm = this.fb.group({
      surname: ['', Validators.required],
      name: ['', Validators.required],
      phone: ['', [Validators.required, Validators.pattern("^0([1-9]([0-9]{2}){3}|[1-9]\.([0-9]{2}\.){3})[0-9]{2}$")]]
    })
  }

  ngOnInit(): void {
  }

  private fetchUser(name: string, surname: string, phone: string): Observable<string> {
    return this.userService.searchUser(1, 0, name, surname, undefined, phone)
      .pipe(map(users => {
        if (users.length == 0) return '';
        return users[0].uuid || "";
      }));
  }

  public onSubmit(): void {
    if (this.slotsGroup === undefined) return;
    const v = this.userForm.getRawValue();
    const userUUID$: Observable<string> = this.fetchUser(v["name"], v["surname"], v["phone"]);
    userUUID$.subscribe(UUID => {
      if (UUID == "") {
        this.userService.register(v["surname"], v["name"], v["phone"], this.randomString(10))
          .subscribe(user => {
            if (user.uuid === undefined) return;
            Swal.fire({
              title: "Succès",
              icon: 'success',
              text: "Utilisateur non trouvé: création automatique",
              position: 'bottom-right',
              timer: 800,
              showConfirmButton: false
            });
            this.userUUID.emit(user['uuid'].toString());
          })
      } else {
        Swal.fire({
          title: "Succès",
          icon: 'success',
          text: "Utilisateur trouvé",
          position: 'bottom-right',
          timer: 800,
          showConfirmButton: false
        });
        this.userUUID.emit(UUID);
      }
    },
      (err) => {
        console.log(err);
      })
  }

  private randomString(length: number): string {
    var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var result = '';
    for (var i = 0; i < length; i++) {
      result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
    }
    return result;
  }
}
