import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchOrCreateUserComponent } from './search-or-create-user.component';

describe('SearchOrCreateUserComponent', () => {
  let component: SearchOrCreateUserComponent;
  let fixture: ComponentFixture<SearchOrCreateUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchOrCreateUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchOrCreateUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
