import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Appointment, AppointmentService, SlotsGroup, SlotsService, UserService } from 'src/app/api';
import { SessionService } from 'src/app/session.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  @Input() forBooking: boolean = true;
  @Input() slotsGroup: SlotsGroup = <SlotsGroup>{};
  @Output() appointmentChanged = new EventEmitter<boolean>();

  public userUUID: string = "";
  public isCollapse: boolean = false;
  public adminReservationIsCollapse: boolean = false;
  public availableSlots: Map<number, number> = new Map();
  public deltaReservation: number = -1;

  constructor(
    private sessionService: SessionService,
    private userService: UserService,
    private appointmentService: AppointmentService,
    private slotsService: SlotsService
  ) { }

  ngOnInit(): void {
    this.setAvailableSlots();
    this.userUUID = this.sessionService.getUserUUID();
  }

  public get quantityLeft(): number {
    if (this.slotsGroup.appointments === undefined) return this.slotsGroup.quantity;
    return this.slotsGroup.quantity - this.slotsGroup.appointments.length;
  }

  public get selfAppointment(): Appointment | undefined {
    if (this.userUUID == "" || this.slotsGroup.appointments == undefined) return
    let appointment = undefined;
    this.slotsGroup.appointments.forEach(a => {
      if (a.user == this.userUUID) {
        appointment = a;
      }
    });
    return appointment;
  }

  public onSubmit() {
    this.book();
  }

  private book(): void {
    if (this.quantityLeft == 0) return;
    if (!this.availableSlots.has(Number(this.deltaReservation))) return;
    this.userService.getUserById(this.userUUID)
      .subscribe(user => {
        if (this.slotsGroup.uuid == undefined) return;
        if (user.uuid == undefined) return;
        this.appointmentService.bookSlot(this.slotsGroup.uuid, user.uuid, this.dateFromDeltaAndStartSlotsGroup(this.deltaReservation).toISOString())
          .subscribe(() => {
            Swal.fire('Réservation réussie');
            this.appointmentChanged.emit(true);
          },
            (err) => {
              if (err instanceof HttpErrorResponse) {
                if (err.error === "appointment already exists\n") {
                  Swal.fire({
                    title: 'Rendez-vous impossible',
                    text: 'Vous avez déjà une réservation sur ce créneau',
                    icon: 'error'
                  });
                }
              }
            })
      })
  }

  public removeSlotsGroup() {
    Swal.fire({
      title: "Suppression de créneaux",
      text: "Attention êtes-vous sûr de vouloir supprimer ces créneaux ?",
      icon: "warning",
      showCancelButton: true,
      showConfirmButton: true
    }).then(value => {
      if (value.isDismissed) {
        console.log("Suppression annulée");
      }
      if (value.isConfirmed) {
        this.slotsService.deleteSlots(this.slotsGroup.uuid)
          .subscribe(() => {
            Swal.fire({
              title: "Suppression effectuée",
              icon: "info",
            }).then(() => {
              this.appointmentChanged.emit(true);
            })
          });
      }
    });
  }

  public unbook() {
    Swal.fire({
      title: "Annuler le rendez-vous",
      text: "Attention êtes-vous sûr de vouloir annuler votre rendez-vous ?",
      icon: "warning",
      showCancelButton: true,
      showConfirmButton: true
    }).then(value => {
      if (value.isConfirmed) {
        this.appointmentService.unbookSlot(this.slotsGroup.uuid, this.userUUID)
          .subscribe(() => {
            Swal.fire({
              title: "Suppression effectuée",
              icon: "info",
            }).then(() => {
              this.appointmentChanged.emit(true);
            })
          });
      }
    });
  }

  public toogleCollapse(): void {
    this.isCollapse = !this.isCollapse;
  }

  public get isAdmin(): boolean {
    return this.sessionService.isAdmin();
  }

  public isThereAppointments(appointments?: Appointment[]): boolean {
    return appointments !== undefined && appointments.length > 0;
  }

  public dateFromDeltaAndStartSlotsGroup(delta: number): Date {
    let value = new Date(this.slotsGroup.from);
    value.setMinutes(value.getMinutes() + +delta);
    return value;
  }

  public dateFromDeltaAndStartSlotsGroupWithoutTimeZone(delta: number): Date {
    let value = new Date(this.slotsGroup.from);
    value.setMinutes(value.getMinutes() + +delta + value.getTimezoneOffset());
    return value;
  }

  private setAvailableSlots(): void {
    if (this.slotsGroup.duration == 0) return;
    const maxNumberOfAvailableSlots = Math.floor(Math.abs(this.deltaTimeSlotsGroup() / this.slotsGroup.duration));
    if (maxNumberOfAvailableSlots < this.slotsGroup.quantity) return;
    for (let i = 0; i < maxNumberOfAvailableSlots; i++) {
      const delta = i * this.slotsGroup.duration;
      const quantityAvailable = this.isSlotsAlreadyBooked(delta)
      if (quantityAvailable > 0) {
        this.availableSlots.set(delta, quantityAvailable);
      }
    }
  }

  private isSlotsAlreadyBooked(delta: number): number {
    let valueToReturn = this.slotsGroup.quantityPerSlot;
    if (this.slotsGroup.appointments == undefined) return valueToReturn;
    this.slotsGroup.appointments.forEach(appointment => {
      if (delta == this.deltaFromStartSlotsGroupToSpecificDate(new Date(appointment.from))) {
        valueToReturn--;
      }
    });
    return valueToReturn;
  }

  private deltaTimeSlotsGroup(): number {
    const to = new Date(this.slotsGroup.to);
    return this.deltaFromStartSlotsGroupToSpecificDate(to);
  }

  private deltaFromStartSlotsGroupToSpecificDate(d: Date): number {
    const from = new Date(this.slotsGroup.from);
    return (d.getHours() - from.getHours()) * 60 + (d.getMinutes() - from.getMinutes());
  }
}
