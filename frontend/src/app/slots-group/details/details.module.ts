import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppointmentsDetailsComponent } from './appointments-details/appointments-details.component';
import { SearchOrCreateUserComponent } from './search-or-create-user/search-or-create-user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DetailsComponent } from './details.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    DetailsComponent,
    AppointmentsDetailsComponent,
    SearchOrCreateUserComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
  ],
  exports: [
    DetailsComponent
  ]
})
export class DetailsModule { }
