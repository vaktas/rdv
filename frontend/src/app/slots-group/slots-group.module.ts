import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { DetailsModule } from './details/details.module';

@NgModule({
  declarations: [
    ListComponent,
  ],
  imports: [
    CommonModule,
    DetailsModule,
  ],
  exports: [
    ListComponent,
  ]
})
export class SlotsGroupModule { }
