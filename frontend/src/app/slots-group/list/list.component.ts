import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SlotsGroup, SlotsService } from 'src/app/api';

export interface SlotsGroupListResult {
  items?: Array<SlotsGroup>;
  item_count?: number;
  current_page?: number;
  items_per_page?: number;
}


@Component({
  selector: 'app-appointment-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnChanges {
  @Input() userUUID: string | undefined;
  @Input() free: boolean | undefined;
  @Input() from: Date | undefined;
  @Input() to: Date | undefined;
  @Input() motif: string | undefined;
  @Output() appointmentBooked = new EventEmitter<boolean>();

  slotsGroups$ = new Observable<SlotsGroupListResult>();

  constructor(
    private slotsService: SlotsService,
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if ((changes['to'] && changes['to'].currentValue !== changes['to'].previousValue) || (changes['from'] && changes['from'].currentValue !== changes['from'].previousValue) || (changes['motif'] && changes['motif'].currentValue !== changes['motif'].previousValue)) {
      this.searchAppointment();
    }
  }

  ngOnInit(): void {
    this.searchAppointment();
  }

  public appointmentTriggered(): void {
    this.appointmentBooked.emit(true);
    this.refreshAppointment();
  }

  public refreshAppointment(): void {
    this.searchAppointment();
  }

  private searchAppointment(): void {
    this.slotsGroups$ = this.slotsService.searchSlots(false, 25, 0, this.userUUID, this.motif, (this.from !== undefined) ? new Date(this.from).toISOString() : undefined, (this.to !== undefined) ? new Date(this.to).toISOString() : undefined, this.free, 'response')
      .pipe(map(response => {
        return <SlotsGroupListResult>{
          items: response.body,
          current_page: 1,
          items_per_page: 20
        }
      }));
  }
}
