module gitlab.com/vaktas/rdv

go 1.19

require (
	github.com/deepmap/oapi-codegen v1.12.2
	github.com/go-chi/chi/v5 v5.0.7
	github.com/google/uuid v1.3.0
	github.com/stretchr/testify v1.8.1
	golang.org/x/crypto v0.1.0
	gorm.io/driver/mysql v1.4.3
	gorm.io/gorm v1.24.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/mattn/go-sqlite3 v1.14.15 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

require (
	github.com/apapsch/go-jsonmerge/v2 v2.0.0 // indirect
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	gorm.io/driver/sqlite v1.4.3
)
