package usecase

import (
	"testing"
	"time"

	"gitlab.com/vaktas/rdv/api"
	"gitlab.com/vaktas/rdv/storage"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var (
	testSlotsGroup = api.SlotsGroup{
		From:            time.Date(2022, 10, 1, 9, 30, 0, 0, time.UTC),
		To:              time.Date(2022, 10, 1, 11, 30, 0, 0, time.UTC),
		Duration:        15,
		Quantity:        5,
		QuantityPerSlot: 2,
		Motif:           motifTestName,
	}
)

func TestCreateAppointment(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.Motif{}, &storage.SlotsGroup{}, &storage.Appointment{}, &storage.User{})
	defer db.Migrator().DropTable(&storage.Motif{}, &storage.SlotsGroup{}, &storage.Appointment{}, &storage.User{})

	storageInstance := &storage.Database{DB: db}
	userManager := UserManager{StorageHandler: storageInstance}
	motifManager := MotifManager{StorageHandler: storageInstance}
	//slotsGroupManager := SlotsGroupManager{StorageHandler: storageInstance}
	//appointmentManager := AppointmentManager{StorageHandler: storageInstance}

	instanciateTestUser(t, &userManager)
	instanciateTestMotif(t, &motifManager)

	//appointmentManager.Create()
}

/*
func instanciateTestSlotsGroup(t *testing.T, manager SlotsGroupManager, storageInstance storage.StorageHandler, slotsGroup *api.SlotsGroup) *api.SlotsGroup {
	manager.Create(api.AddSlotsParams{
		Motif:           testSlotsGroup.Motif,
		From:            testSlotsGroup.From,
		To:              testSlotsGroup.To,
		Quantity:        testSlotsGroup.Quantity,
		QuantityPerSlot: testSlotsGroup.QuantityPerSlot,
		Duration:        testSlotsGroup.Duration,
	})
	slotsGroups, max, err := storageInstance.FindSlotsGroups(1, 0, nil, nil, nil, true)
	assert.Nil(t, err)
	assert.Equal(t, 1, max)

	return slo
}
*/
