package usecase

import (
	"log"
	"time"

	"github.com/google/uuid"
	"gitlab.com/vaktas/rdv/api"
	"gitlab.com/vaktas/rdv/storage"
)

type TokenManager struct {
	StorageHandler storage.StorageHandler
}

func (m *TokenManager) Create(user *api.User) (tokeId uuid.UUID, err error) {
	token, err := m.StorageHandler.CreateToken(user.Uuid)
	if err != nil {
		if _, ok := err.(*storage.TokenNotExpired); !ok {
			return
		}
		log.Println("token already assigned")
	}
	return token.ID, nil
}

func (m *TokenManager) GetUser(tokenID uuid.UUID) (user *api.User, err error) {
	token, err := m.StorageHandler.FindToken(tokenID)
	if err != nil {
		return
	}
	if token.To.Unix() < time.Now().Unix() {
		err = &storage.TokenNotExpired{}
		return
	}
	return storage.UserToUserEntity(&token.User), nil
}

func (m *TokenManager) DeleteByUserUUID(userUUID uuid.UUID) (err error) {
	err = m.StorageHandler.DeleteTokenByUserUUID(userUUID)
	if err != nil {
		return
	}
	return
}
