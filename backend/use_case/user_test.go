package usecase

import (
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/vaktas/rdv/api"
	"gitlab.com/vaktas/rdv/storage"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

const (
	testSurname     = "test-sur"
	testName        = "test-first"
	testPhoneNumber = "0600000000"
	testPassword    = "0600000000"
)

func TestCreateUser(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.User{}, &storage.Appointment{})
	defer db.Migrator().DropTable(&storage.User{}, &storage.Appointment{})

	storagetInstance := &storage.Database{DB: db}
	manager := UserManager{StorageHandler: storagetInstance}

	user, err := manager.Create(api.RegisterParams{
		Surname:  testSurname,
		Name:     testName,
		Tel:      testPhoneNumber,
		Password: testPassword,
	})
	assert.Nil(t, err)
	assert.NotEqualValues(t, user.Uuid, uuid.Nil)
	assert.EqualValues(t, user.Surname, testSurname)
	assert.EqualValues(t, user.Name, testName)
	assert.EqualValues(t, user.Tel, testPhoneNumber)
	assert.EqualValues(t, user.Login, fmt.Sprintf("%s_%s", testSurname, testName))
}

func TestCreateUserSameNameAndSurname(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.User{}, &storage.Appointment{})
	defer db.Migrator().DropTable(&storage.User{}, &storage.Appointment{})

	storagetInstance := &storage.Database{DB: db}
	manager := UserManager{StorageHandler: storagetInstance}

	user, err := manager.Create(api.RegisterParams{
		Surname:  testSurname,
		Name:     testName,
		Tel:      testPhoneNumber,
		Password: testPassword,
	})
	assert.Nil(t, err)
	assert.NotEqualValues(t, user.Uuid, uuid.Nil)
	assert.EqualValues(t, user.Surname, testSurname)
	assert.EqualValues(t, user.Name, testName)
	assert.EqualValues(t, user.Tel, testPhoneNumber)
	assert.EqualValues(t, user.Login, fmt.Sprintf("%s_%s", testSurname, testName))

	user, err = manager.Create(api.RegisterParams{
		Surname:  testSurname,
		Name:     testName,
		Tel:      testPhoneNumber,
		Password: testPassword,
	})
	assert.Nil(t, err)
	assert.NotEqualValues(t, user.Uuid, uuid.Nil)
	assert.EqualValues(t, user.Surname, testSurname)
	assert.EqualValues(t, user.Name, testName)
	assert.EqualValues(t, user.Tel, testPhoneNumber)
	assert.EqualValues(t, user.Login, fmt.Sprintf("%s_%s_0", testSurname, testName))
}

func TestGetUser(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.User{}, &storage.Appointment{})
	defer db.Migrator().DropTable(&storage.User{}, &storage.Appointment{})

	storagetInstance := &storage.Database{DB: db}
	manager := UserManager{StorageHandler: storagetInstance}

	createdUser, err := manager.Create(api.RegisterParams{
		Surname:  testSurname,
		Name:     testName,
		Tel:      testPhoneNumber,
		Password: testPassword,
	})
	assert.Nil(t, err)
	assert.NotEqualValues(t, createdUser.Uuid, uuid.Nil)
	assert.EqualValues(t, createdUser.Surname, testSurname)
	assert.EqualValues(t, createdUser.Name, testName)
	assert.EqualValues(t, createdUser.Tel, testPhoneNumber)
	assert.EqualValues(t, createdUser.Login, fmt.Sprintf("%s_%s", testSurname, testName))

	user, err := manager.Get(createdUser.Uuid)
	assert.Nil(t, err)
	assert.Equal(t, createdUser, user)
}

func TestGetUserUnknown(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.User{}, &storage.Appointment{})
	defer db.Migrator().DropTable(&storage.User{}, &storage.Appointment{})

	storagetInstance := &storage.Database{DB: db}
	manager := UserManager{StorageHandler: storagetInstance}

	_, err := manager.Get(uuid.New())
	assert.Equal(t, &NotFoundError{}, err)
}

func TestGetUserByLogin(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.User{}, &storage.Appointment{})
	defer db.Migrator().DropTable(&storage.User{}, &storage.Appointment{})

	storagetInstance := &storage.Database{DB: db}
	manager := UserManager{StorageHandler: storagetInstance}

	createdUser, err := manager.Create(api.RegisterParams{
		Surname:  testSurname,
		Name:     testName,
		Tel:      testPhoneNumber,
		Password: testPassword,
	})
	assert.Nil(t, err)
	assert.NotEqualValues(t, createdUser.Uuid, uuid.Nil)
	assert.EqualValues(t, createdUser.Surname, testSurname)
	assert.EqualValues(t, createdUser.Name, testName)
	assert.EqualValues(t, createdUser.Tel, testPhoneNumber)
	assert.EqualValues(t, createdUser.Login, fmt.Sprintf("%s_%s", testSurname, testName))

	user, err := manager.GetByLogin(createdUser.Login)
	assert.Nil(t, err)
	assert.Equal(t, createdUser, user)
}

func TestGetUserByLoginUnknown(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.User{}, &storage.Appointment{})
	defer db.Migrator().DropTable(&storage.User{}, &storage.Appointment{})

	storagetInstance := &storage.Database{DB: db}
	manager := UserManager{StorageHandler: storagetInstance}

	_, err := manager.GetByLogin("test123")
	assert.Equal(t, &NotFoundError{}, err)
}

func TestGetUserRole(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.User{}, &storage.Appointment{}, &storage.Role{})
	defer db.Migrator().DropTable(&storage.User{}, &storage.Appointment{}, &storage.Role{})

	storagetInstance := &storage.Database{DB: db}
	manager := UserManager{StorageHandler: storagetInstance}

	db.Create(&storage.Role{
		Name: "user",
	}).Commit()

	user, err := manager.Create(api.RegisterParams{
		Surname:  testSurname,
		Name:     testName,
		Tel:      testPhoneNumber,
		Password: testPassword,
	})
	assert.Nil(t, err)
	assert.NotEqualValues(t, user.Uuid, uuid.Nil)
	assert.EqualValues(t, user.Surname, testSurname)
	assert.EqualValues(t, user.Name, testName)
	assert.EqualValues(t, user.Tel, testPhoneNumber)
	assert.EqualValues(t, user.Login, fmt.Sprintf("%s_%s", testSurname, testName))

	role, err := manager.GetRole(user.Uuid)
	assert.Nil(t, err)
	assert.Equal(t, api.USER, role)
}

func TestGetUserRoleUnknown(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.User{}, &storage.Appointment{}, &storage.Role{})
	defer db.Migrator().DropTable(&storage.User{}, &storage.Appointment{}, &storage.Role{})

	storagetInstance := &storage.Database{DB: db}
	manager := UserManager{StorageHandler: storagetInstance}

	db.Create(&storage.Role{
		Name: "user",
	}).Commit()

	_, err := manager.GetRole(uuid.New())
	assert.Equal(t, &NotFoundError{}, err)
}

func instanciateTestUser(t *testing.T, manager *UserManager) {
	user, err := manager.Create(api.RegisterParams{
		Surname:  testSurname,
		Name:     testName,
		Tel:      testPhoneNumber,
		Password: testPassword,
	})
	assert.Nil(t, err)
	assert.NotEqualValues(t, user.Uuid, uuid.Nil)
}
