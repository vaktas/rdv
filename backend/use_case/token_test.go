package usecase

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/vaktas/rdv/api"
	"gitlab.com/vaktas/rdv/storage"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func TestCreateToken(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.Token{}, &storage.User{})
	defer db.Migrator().DropTable(&storage.Token{}, &storage.User{})

	storagetInstance := &storage.Database{DB: db}
	userManager := UserManager{StorageHandler: storagetInstance}
	tokenManager := TokenManager{StorageHandler: storagetInstance}

	user, err := userManager.Create(api.RegisterParams{
		Surname:  "test",
		Name:     "test",
		Tel:      "0600000000",
		Password: "azerty",
	})
	assert.Nil(t, err)

	token, err := tokenManager.Create(user)
	assert.Nil(t, err)
	assert.NotEqual(t, uuid.Nil, token)
}

func TestCreateTokenAlreadyExist(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.Token{}, &storage.User{})
	defer db.Migrator().DropTable(&storage.Token{}, &storage.User{})

	storagetInstance := &storage.Database{DB: db}
	userManager := UserManager{StorageHandler: storagetInstance}
	tokenManager := TokenManager{StorageHandler: storagetInstance}

	user, err := userManager.Create(api.RegisterParams{
		Surname:  "test",
		Name:     "test",
		Tel:      "0600000000",
		Password: "azerty",
	})
	assert.Nil(t, err)

	assignedToken, err := tokenManager.Create(user)
	assert.Nil(t, err)
	assert.NotEqual(t, uuid.Nil, assignedToken)

	token, err := tokenManager.Create(user)
	assert.Nil(t, err)
	assert.Equal(t, assignedToken, token)
}

func TestDeleteToken(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.Token{}, &storage.User{})
	defer db.Migrator().DropTable(&storage.Token{}, &storage.User{})

	storagetInstance := &storage.Database{DB: db}
	userManager := UserManager{StorageHandler: storagetInstance}
	tokenManager := TokenManager{StorageHandler: storagetInstance}

	user, err := userManager.Create(api.RegisterParams{
		Surname:  "test",
		Name:     "test",
		Tel:      "0600000000",
		Password: "azerty",
	})
	assert.Nil(t, err)

	token, err := tokenManager.Create(user)
	assert.Nil(t, err)
	assert.NotEqual(t, uuid.Nil, token)

	err = tokenManager.DeleteByUserUUID(user.Uuid)
	assert.Nil(t, err)
}

func TestDeleteTokenNotExisting(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.Token{}, &storage.User{})
	defer db.Migrator().DropTable(&storage.Token{}, &storage.User{})

	storagetInstance := &storage.Database{DB: db}
	userManager := UserManager{StorageHandler: storagetInstance}
	tokenManager := TokenManager{StorageHandler: storagetInstance}

	user, err := userManager.Create(api.RegisterParams{
		Surname:  "test",
		Name:     "test",
		Tel:      "0600000000",
		Password: "azerty",
	})
	assert.Nil(t, err)

	err = tokenManager.DeleteByUserUUID(user.Uuid)
	assert.NotNil(t, err)
}
