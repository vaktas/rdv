package usecase

import (
	"fmt"
	"math"
	"time"

	"gitlab.com/vaktas/rdv/api"
	"gitlab.com/vaktas/rdv/storage"
)

type AppointmentManager struct {
	StorageHandler storage.StorageHandler
}

func (m *AppointmentManager) Create(slotsGroupUUID api.PathSlotsGroupUUID, params api.BookSlotParams) error {
	filter := &storage.SlotsGroup{}
	filter.UUID = slotsGroupUUID
	slotsGroups, _, err := m.StorageHandler.FindSlotsGroups(1, 0, filter, nil, nil, true)
	if err != nil {
		return err
	}
	if len(slotsGroups) != 1 {
		return &NotFoundError{}
	}

	slotsGroup := slotsGroups[0]
	if len(slotsGroup.Appointments) >= int(slotsGroup.Quantity)*int(slotsGroup.Duration) {
		return fmt.Errorf("appointment not available")
	}

	slotsExist := false
	for i := 0; i < int(math.Floor(float64((slotsGroup.To.Unix()-slotsGroup.From.Unix())/60)/float64(slotsGroup.Duration))); i++ {
		_ = slotsGroup.From.Add(time.Duration(slotsGroup.Duration*uint(i)) * time.Minute)
		if slotsGroup.From.Add(time.Duration(slotsGroup.Duration*uint(i))*time.Minute).Unix() == time.Time(params.At).Unix() {
			slotsExist = true
			break
		}
	}
	if !slotsExist {
		return &NotFoundError{}
	}

	numberSLotsTaken := 0
	for _, a := range slotsGroup.Appointments {
		if time.Time(params.At).Unix() == a.At.Unix() {
			numberSLotsTaken++
		}
	}
	if numberSLotsTaken >= int(slotsGroup.QuantityPerSlots) {
		return fmt.Errorf("appointment not available")
	}

	users, _, err := m.StorageHandler.FindUsers(1, 0, &storage.User{
		UUID: params.UserUUID,
	})
	if err != nil {
		return err
	}
	if len(users) != 1 {
		return &NotFoundError{}
	}

	userBooking := users[0]
	for _, a := range userBooking.Appointments {
		if a.SlotsGroupUUID == slotsGroup.UUID {
			return &storage.AppointmentAlreadyExists{}
		}
		// TODO: Get the slotsGroup of the appointment a in order to add the duration
		// The Function that find a SlotsGroup by its UUID is needed
		if a.At.Unix() < time.Time(params.At).Add(time.Duration(slotsGroup.Duration)*time.Minute).Unix() && time.Time(params.At).Unix() < a.At.Add(time.Minute*time.Duration(slotsGroup.Duration)).Unix() {
			return &storage.AppointmentAlreadyExists{}
		}
	}
	return m.StorageHandler.CreateAppointment(slotsGroup, userBooking, time.Time(params.At))
}

func (m *AppointmentManager) Delete(slotsGroupUUID api.PathSlotsGroupUUID, params api.UnbookSlotParams) error {
	return m.StorageHandler.RemoveAppointment(slotsGroupUUID, params.UserUUID)
}

func (m *AppointmentManager) Validate(slotsGroupUUID api.PathSlotsGroupUUID, userUUID api.PathUserUUID) error {
	return m.StorageHandler.UpdateAppointment(storage.Appointment{
		SlotsGroupUUID: slotsGroupUUID,
		UserUUID:       userUUID,
		Done:           true,
	})
}
