package usecase

import (
	"fmt"

	"gitlab.com/vaktas/rdv/api"
	"gitlab.com/vaktas/rdv/storage"
)

type MotifManager struct {
	StorageHandler storage.StorageHandler
}

func (s *MotifManager) Search(params api.SearchMotifParams) (motifs []api.Motif, err error) {
	var motif *string
	if params.Name != nil {
		val := string(*params.Name)
		motif = &val
	}
	foundMotifs, err := s.StorageHandler.FindMotifs(motif, params.Used)
	if err != nil {
		return
	}

	for _, m := range foundMotifs {
		motifs = append(motifs, *storage.MotifToMotifEntity(m))
	}
	return
}

func (s *MotifManager) Create(params api.AddMotifParams) (err error) {
	motif, err := s.StorageHandler.FindMotifByName(params.Name)
	if motif.ID != 0 {
		err = fmt.Errorf("nom déjà existant pour ce motif")
	}
	if err != nil {
		return
	}
	return s.StorageHandler.CreateMotif(params.Name)
}

func (s *MotifManager) Update(params api.PutMotifParams) (err error) {
	motif, err := s.StorageHandler.FindMotifByName(params.Old)
	if motif.ID == 0 {
		err = &NotFoundError{}
	}
	if err != nil {
		return err
	}
	motif, err = s.StorageHandler.FindMotifByName(params.New)
	if motif.ID != 0 {
		err = fmt.Errorf("nom déjà existant pour ce motif")
	}
	if err != nil {
		return
	}
	return s.StorageHandler.UpdateMotif(params.Old, params.New)
}

func (s *MotifManager) Delete(params api.DeleteMotifParams) (err error) {
	motif, err := s.StorageHandler.FindMotifByName(params.Name)
	if motif.ID == 0 {
		err = &NotFoundError{}
	}
	if err != nil {
		return
	}
	return s.StorageHandler.DeleteMotif(params.Name)
}
