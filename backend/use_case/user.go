package usecase

import (
	"github.com/google/uuid"
	"gitlab.com/vaktas/rdv/api"
	"gitlab.com/vaktas/rdv/storage"
)

type UserManager struct {
	StorageHandler storage.StorageHandler
}

func (m *UserManager) Create(params api.RegisterParams) (*api.User, error) {
	user, err := m.StorageHandler.CreateUser(string(params.Surname), string(params.Name), string(params.Tel), string(params.Password))
	if err != nil {
		return nil, err
	}
	return storage.UserToUserEntity(user), nil
}

func (m *UserManager) Search(params api.SearchUserParams) (users []api.User, max int, err error) {
	filter := &storage.User{}
	if params.Name != nil {
		filter.Name = string(*params.Name)
	}
	if params.Surname != nil {
		filter.Surname = string(*params.Surname)
	}
	if params.Login != nil {
		filter.Login = string(*params.Login)
	}
	if params.Phone != nil {
		filter.Telephone = string(*params.Phone)
	}
	if params.Login == nil && params.Name == nil && params.Surname == nil && params.Phone == nil {
		filter = nil
	}
	limit := 25
	if params.Limit != nil {
		limit = int(*params.Limit)
	}
	offset := 0
	if params.Offset != nil {
		offset = int(*params.Offset)
	}
	foundUsers, max, err := m.StorageHandler.FindUsers(uint(limit), uint(offset), filter)
	if err != nil {
		return
	}

	for _, u := range foundUsers {
		users = append(users, *storage.UserToUserEntity(&u))
	}
	return
}

func (m *UserManager) Get(userUUID uuid.UUID) (user *api.User, err error) {
	foundUser, err := m.StorageHandler.FindUser(userUUID)
	if foundUser == nil || foundUser.UUID == uuid.Nil {
		err = &NotFoundError{}
	}
	if err != nil {
		return
	}
	user = storage.UserToUserEntity(foundUser)
	return
}

func (m *UserManager) GetByLogin(login string) (user *api.User, err error) {
	users, _, err := m.StorageHandler.FindUsers(uint(1), uint(0), &storage.User{Login: login})
	if len(users) == 0 {
		err = &NotFoundError{}
	}
	if err != nil {
		return
	}
	user = storage.UserToUserEntity(&users[0])
	return
}

func (m *UserManager) GetRole(userUUID uuid.UUID) (role string, err error) {
	user, err := m.StorageHandler.FindUser(userUUID)
	if user == nil || user.UUID == uuid.Nil {
		err = &NotFoundError{}
	}
	if err != nil {
		return
	}
	return user.Role.Name, nil
}

func (m *UserManager) Login(params api.LoginParams) (err error) {
	_, err = m.StorageHandler.GetUserByLoginAndPassword(string(params.Username), params.Password)
	return
}
