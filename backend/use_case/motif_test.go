package usecase

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/vaktas/rdv/api"
	"gitlab.com/vaktas/rdv/storage"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

const motifTestName = "test"

var nilMotif = &storage.Motif{}

func TestAddMotif(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.Motif{})
	defer db.Migrator().DropTable(&storage.Motif{})

	storagetInstance := &storage.Database{DB: db}
	manager := MotifManager{StorageHandler: storagetInstance}

	motif, err := storagetInstance.FindMotifByName(motifTestName)
	assert.Nil(t, err)
	assert.EqualValues(t, nilMotif, motif)

	err = manager.Create(api.AddMotifParams{Name: motifTestName})
	assert.Nil(t, err)

	motif, err = storagetInstance.FindMotifByName(motifTestName)
	assert.Nil(t, err)
	assert.EqualValues(t, motifTestName, motif.Name)
}

func TestAddMotifAlreadyExists(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.Motif{})
	defer db.Migrator().DropTable(&storage.Motif{})

	storagetInstance := &storage.Database{DB: db}
	manager := MotifManager{StorageHandler: storagetInstance}

	motif, err := storagetInstance.FindMotifByName(motifTestName)
	assert.Nil(t, err)
	assert.EqualValues(t, nilMotif, motif)

	err = manager.Create(api.AddMotifParams{Name: motifTestName})
	assert.Nil(t, err)

	motif, err = storagetInstance.FindMotifByName(motifTestName)
	assert.Nil(t, err)
	assert.EqualValues(t, motifTestName, motif.Name)

	err = manager.Create(api.AddMotifParams{Name: motifTestName})
	assert.EqualError(t, err, "nom déjà existant pour ce motif")
}

func TestAddMotifEmptyName(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.Motif{})
	defer db.Migrator().DropTable(&storage.Motif{})

	storagetInstance := &storage.Database{DB: db}
	manager := MotifManager{StorageHandler: storagetInstance}

	motif, err := storagetInstance.FindMotifByName("")
	assert.Nil(t, err)
	assert.EqualValues(t, nilMotif, motif)

	err = manager.Create(api.AddMotifParams{Name: ""})
	assert.EqualError(t, err, "name of a motif cannot be empty")
}

func TestSearchMotif(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.Motif{})
	defer db.Migrator().DropTable(&storage.Motif{})

	storagetInstance := &storage.Database{DB: db}
	manager := MotifManager{StorageHandler: storagetInstance}

	err := manager.Create(api.AddMotifParams{Name: motifTestName})
	assert.Nil(t, err)

	motifName := motifTestName
	motifs, err := manager.Search(api.SearchMotifParams{Name: &motifName})
	assert.Nil(t, err)
	assert.Len(t, motifs, 1)
}

func TestSearchMotifMultipleStored(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.Motif{})
	defer db.Migrator().DropTable(&storage.Motif{})

	storagetInstance := &storage.Database{DB: db}
	manager := MotifManager{StorageHandler: storagetInstance}

	err := manager.Create(api.AddMotifParams{Name: motifTestName})
	assert.Nil(t, err)

	for i := range []int{0, 1, 2} {
		err := manager.Create(api.AddMotifParams{Name: fmt.Sprintf("%s-%d", motifTestName, i)})
		assert.Nil(t, err)
	}

	motifName := motifTestName
	motifs, err := manager.Search(api.SearchMotifParams{Name: &motifName})
	assert.Nil(t, err)
	assert.Len(t, motifs, 1)
}

func TestSearchMotifUnknown(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.Motif{})
	defer db.Migrator().DropTable(&storage.Motif{})

	storagetInstance := &storage.Database{DB: db}
	manager := MotifManager{StorageHandler: storagetInstance}

	for i := range []int{0, 1, 2} {
		err := manager.Create(api.AddMotifParams{Name: fmt.Sprintf("%s-%d", motifTestName, i)})
		assert.Nil(t, err)
	}

	motifName := motifTestName
	motifs, err := manager.Search(api.SearchMotifParams{Name: &motifName})
	assert.Nil(t, err)
	assert.Len(t, motifs, 0)
}

func TestUpdateMotif(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.Motif{})
	defer db.Migrator().DropTable(&storage.Motif{})

	storagetInstance := &storage.Database{DB: db}
	manager := MotifManager{StorageHandler: storagetInstance}

	err := manager.Create(api.AddMotifParams{Name: motifTestName})
	assert.Nil(t, err)

	err = manager.Update(api.PutMotifParams{Old: motifTestName, New: "newName"})
	assert.Nil(t, err)
}

func TestUpdateMotifUnknown(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.Motif{})
	defer db.Migrator().DropTable(&storage.Motif{})

	storagetInstance := &storage.Database{DB: db}
	manager := MotifManager{StorageHandler: storagetInstance}

	err := manager.Update(api.PutMotifParams{Old: motifTestName, New: "newName"})
	assert.Equal(t, &NotFoundError{}, err)
}

func TestDeleteMotif(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.Motif{})
	defer db.Migrator().DropTable(&storage.Motif{})

	storagetInstance := &storage.Database{DB: db}
	manager := MotifManager{StorageHandler: storagetInstance}

	err := manager.Create(api.AddMotifParams{Name: motifTestName})
	assert.Nil(t, err)

	err = manager.Delete(api.DeleteMotifParams{Name: motifTestName})
	assert.Nil(t, err)
}

func TestDeleteMotifUnknown(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.Migrator().CreateTable(&storage.Motif{})
	defer db.Migrator().DropTable(&storage.Motif{})

	storagetInstance := &storage.Database{DB: db}
	manager := MotifManager{StorageHandler: storagetInstance}

	err := manager.Delete(api.DeleteMotifParams{Name: motifTestName})
	assert.Equal(t, &NotFoundError{}, err)
}

func instanciateTestMotif(t *testing.T, manager *MotifManager) {
	err := manager.Create(api.AddMotifParams{Name: motifTestName})
	assert.Nil(t, err)
}
