package usecase

import (
	"time"

	"gitlab.com/vaktas/rdv/api"
	"gitlab.com/vaktas/rdv/storage"
)

type SlotsGroupManager struct {
	StorageHandler storage.StorageHandler
}

func (m *SlotsGroupManager) Create(params api.AddSlotsParams) (err error) {
	return m.StorageHandler.CreateSlotsGroup(time.Time(params.From), time.Time(params.To), string(params.Motif), uint(params.QuantityPerSlot), uint(params.Quantity), uint(params.Duration))
}

func (m *SlotsGroupManager) Search(params api.SearchSlotsParams, loggedUser *api.User, role string) (slotsGroups []api.SlotsGroup, max int, err error) {
	limit := 25
	offset := 0
	filter := &storage.SlotsGroup{}
	filter2 := &storage.User{}

	if params.Limit != nil {
		limit = int(*params.Limit)
	}
	if params.Offset != nil {
		offset = int(*params.Offset)
	}
	if params.UserUUID != nil {
		foundUser, _ := m.StorageHandler.FindUser(*params.UserUUID)
		filter2 = foundUser
	}
	if params.From != nil {
		filter.From = time.Time(*params.From)
	}
	if params.To != nil {
		filter.To = time.Time(*params.To)
	}
	if params.Motif != nil {
		motif, _ := m.StorageHandler.FindMotifByName(string(*params.Motif))
		filter.Motif = *motif
	}
	if params.Motif == nil && params.From == nil && params.To == nil {
		filter = nil
	}
	if params.UserUUID == nil {
		filter2 = nil
	}
	free := false
	if params.Free != nil {
		free = *params.Free
	}
	foundGroups, max, err := m.StorageHandler.FindSlotsGroups(uint(limit), uint(offset), filter, filter2, nil, free)
	if err != nil {
		return
	}

	for _, slot := range foundGroups {
		slotsGroup := storage.SlotsGroupToSlotsGroupEntity(slot, *loggedUser, role, filter2 != nil, free)
		if slotsGroup != nil {
			slotsGroups = append(slotsGroups, *slotsGroup)
		}
	}
	return
}

func (m *SlotsGroupManager) Delete(slotsGroupUUID api.PathSlotsGroupUUID, params api.DeleteSlotsParams) (err error) {
	return m.StorageHandler.DeleteSlotsGroup(slotsGroupUUID)
}
