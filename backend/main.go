package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"
	api "gitlab.com/vaktas/rdv/api"
	http_endpoints "gitlab.com/vaktas/rdv/interface_adapter/http_endpoints"
	"gitlab.com/vaktas/rdv/storage"
)

func main() {
	db, err := storage.NewDatabase()
	if err != nil {
		panic(err)
	}
	tx := db.DB.Begin()
	adminRole := storage.Role{
		Name: "admin",
	}
	userRole := storage.Role{
		Name: "user",
	}
	tx.Create(&adminRole)
	tx.Create(&userRole)
	tx.Commit()

	database := &storage.Database{DB: db.DB}

	r := chi.NewRouter()

	srv, err := http_endpoints.NewServer(database)
	if err != nil {
		panic(err)
	}
	api.HandlerFromMuxWithBaseURL(srv, r, "/v1")

	s := &http.Server{
		Handler: r,
		Addr:    fmt.Sprintf("0.0.0.0:%d", 8080),
	}

	log.Fatal(s.ListenAndServe())
}
