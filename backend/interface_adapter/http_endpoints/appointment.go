package httpendpoints

import (
	"fmt"
	"net/http"

	api "gitlab.com/vaktas/rdv/api"
)

func (s *ServerImplementation) SearchSlots(w http.ResponseWriter, r *http.Request, params api.SearchSlotsParams) {
	user, role, err := getUserAndRoleFromRequest(r, s.userManager, s.tokenManager)
	if err != nil || (role != api.ADMIN && (params.Free != nil && !*params.Free || (params.UserUUID != nil && *params.UserUUID != user.Uuid))) {
		sendErrorNotAuthorize(w, fmt.Errorf("this action is not authorized"))
		return
	}
	slotsGroups, max, err := s.slotsGroupManager.Search(params, user, role)
	if err != nil {
		sendErrorBadRequest(w, err)
		return
	}
	w.Header().Set("X-Total-Count", fmt.Sprintf("%d", max))
	sendOkResponse(w, slotsGroups)
}

func (s *ServerImplementation) AddSlots(w http.ResponseWriter, r *http.Request, params api.AddSlotsParams) {
	_, role, err := getUserAndRoleFromRequest(r, s.userManager, s.tokenManager)
	if err != nil {
		sendErrorBadRequest(w, err)
		return
	}
	if err != nil || role != api.ADMIN {
		sendErrorNotAuthorize(w, err)
		return
	}
	err = s.slotsGroupManager.Create(params)
	if err != nil {
		sendErrorBadRequest(w, err)
		return
	}
	sendOkResponse(w, nil)
}

func (s *ServerImplementation) DeleteSlots(w http.ResponseWriter, r *http.Request, slotsGroupUUID api.PathSlotsGroupUUID, params api.DeleteSlotsParams) {
	_, role, err := getUserAndRoleFromRequest(r, s.userManager, s.tokenManager)
	if err != nil {
		sendErrorBadRequest(w, err)
		return
	}
	if role != api.ADMIN {
		sendErrorNotAuthorize(w, err)
		return
	}

	err = s.slotsGroupManager.Delete(slotsGroupUUID, params)
	if err != nil {
		sendErrorBadRequest(w, err)
		return
	}
	sendOkResponse(w, nil)
}
func (s *ServerImplementation) GetSlots(w http.ResponseWriter, r *http.Request, slotsGroupUUID api.PathSlotsGroupUUID, params api.GetSlotsParams) {
	_, _, err := getUserAndRoleFromRequest(r, s.userManager, s.tokenManager)
	if err != nil {
		sendErrorNotAuthorize(w, err)
	}
}
func (s *ServerImplementation) BookSlot(w http.ResponseWriter, r *http.Request, slotsGroupUUID api.PathSlotsGroupUUID, params api.BookSlotParams) {
	user, role, err := getUserAndRoleFromRequest(r, s.userManager, s.tokenManager)
	if err != nil {
		sendErrorBadRequest(w, err)
		return
	}
	if role == api.USER && user.Uuid != params.UserUUID {
		sendErrorNotAuthorize(w, fmt.Errorf("you don't have the right role"))
		return
	}

	err = s.appointmentManager.Create(slotsGroupUUID, params)
	if err != nil {
		sendErrorBadRequest(w, err)
		return
	}
	sendOkResponse(w, nil)
}
func (s *ServerImplementation) UnbookSlot(w http.ResponseWriter, r *http.Request, slotsGroupUUID api.PathSlotsGroupUUID, params api.UnbookSlotParams) {
	user, role, err := getUserAndRoleFromRequest(r, s.userManager, s.tokenManager)
	if err != nil {
		sendErrorBadRequest(w, err)
		return
	}
	if role == api.USER && user.Uuid != params.UserUUID {
		sendErrorNotAuthorize(w, fmt.Errorf("unauthorized"))
		return
	}
	err = s.appointmentManager.Delete(slotsGroupUUID, params)
	if err != nil {
		sendErrorBadRequest(w, err)
		return
	}
	sendOkResponse(w, nil)
}

func (s *ServerImplementation) CheckAppointment(w http.ResponseWriter, r *http.Request, slotsGroupUUID api.PathSlotsGroupUUID, userUUID api.PathUserUUID) {
	_, role, err := getUserAndRoleFromRequest(r, s.userManager, s.tokenManager)
	if err != nil {
		sendErrorBadRequest(w, err)
		return
	}
	if role == api.USER {
		sendErrorNotAuthorize(w, fmt.Errorf("unauthorized"))
		return
	}
	err = s.appointmentManager.Validate(slotsGroupUUID, userUUID)
	if err != nil {
		sendErrorBadRequest(w, err)
		return
	}
	sendOkResponse(w, nil)
}

func (s *ServerImplementation) GetAppointment(w http.ResponseWriter, r *http.Request, slotsGroupUUID api.PathSlotsGroupUUID, userUUID api.PathUserUUID) {

}
