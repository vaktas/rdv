package httpendpoints

import (
	"encoding/json"
	"net/http"
)

func sendErrorNotAuthorize(w http.ResponseWriter, err error) {
	http.Error(w, err.Error(), http.StatusUnauthorized)
}
func sendErrorBadRequest(w http.ResponseWriter, err error) {
	http.Error(w, err.Error(), http.StatusBadRequest)
}
func sendOkResponse(w http.ResponseWriter, a interface{}) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	json.NewEncoder(w).Encode(a)
}
