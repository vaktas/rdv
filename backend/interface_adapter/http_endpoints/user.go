package httpendpoints

import (
	"fmt"
	"log"
	"net/http"

	"github.com/google/uuid"
	api "gitlab.com/vaktas/rdv/api"
	"gitlab.com/vaktas/rdv/storage"
)

func (s *ServerImplementation) Login(w http.ResponseWriter, r *http.Request, params api.LoginParams) {
	if user, _, _ := getUserAndRoleFromRequest(r, s.userManager, s.tokenManager); user != nil && user.Uuid != uuid.Nil {
		sendErrorBadRequest(w, fmt.Errorf(""))
		return
	}

	err := s.userManager.Login(params)
	if err != nil {
		sendErrorNotAuthorize(w, err)
		return
	}

	user, err := s.userManager.GetByLogin(params.Username)
	if err != nil {
		sendErrorNotAuthorize(w, err)
		return
	}

	role, err := s.userManager.GetRole(user.Uuid)
	if err != nil {
		sendErrorNotAuthorize(w, err)
		return
	}

	token, err := s.tokenManager.Create(user)
	if err != nil {
		if _, ok := err.(*storage.TokenNotExpired); !ok {
			sendErrorBadRequest(w, err)
			return
		}
		log.Println("token already assigned")
	}

	type loginResult struct {
		Token string `json:"token,omitempty"`
		Role  string `json:"role,omitempty"`
		UUID  string `json:"uuid,omitempty"`
	}

	log.Default().Println(role)

	sendOkResponse(w, loginResult{
		Token: token.String(),
		Role:  role,
		UUID:  user.Uuid.String(),
	})
}

func (s *ServerImplementation) Logout(w http.ResponseWriter, r *http.Request) {
	user, _, err := getUserAndRoleFromRequest(r, s.userManager, s.tokenManager)
	if err != nil {
		sendErrorNotAuthorize(w, err)
		return
	}
	err = s.tokenManager.DeleteByUserUUID(user.Uuid)
	if err != nil {
		sendErrorNotAuthorize(w, err)
		return
	}
	sendOkResponse(w, nil)
}

func (s *ServerImplementation) Register(w http.ResponseWriter, r *http.Request, params api.RegisterParams) {
	user, role, _ := getUserAndRoleFromRequest(r, s.userManager, s.tokenManager)
	if user != nil && user.Uuid != uuid.Nil && role != api.ADMIN {
		sendErrorBadRequest(w, fmt.Errorf("cannot register the user"))
		return
	}
	user, err := s.userManager.Create(params)
	if err != nil {
		sendErrorBadRequest(w, err)
		return
	}
	sendOkResponse(w, user)
}

func (s *ServerImplementation) SearchUser(w http.ResponseWriter, r *http.Request, params api.SearchUserParams) {
	user, role, err := getUserAndRoleFromRequest(r, s.userManager, s.tokenManager)
	if err != nil {
		sendErrorBadRequest(w, err)
		return
	}
	if role == api.USER && params.Login != nil && *params.Login != user.Login {
		sendErrorNotAuthorize(w, fmt.Errorf("not allowed to search for this user"))
		return
	}
	users, max, err := s.userManager.Search(params)
	if err != nil {
		sendErrorBadRequest(w, err)
		return
	}
	w.Header().Set("X-Total-Count", fmt.Sprintf("%d", max))
	sendOkResponse(w, users)
}

func (s *ServerImplementation) GetUserById(w http.ResponseWriter, r *http.Request, userUUID api.PathUserUUID) {
	user, role, err := getUserAndRoleFromRequest(r, s.userManager, s.tokenManager)
	if err != nil {
		sendErrorBadRequest(w, err)
		return
	}
	if user.Uuid != userUUID && role != api.ADMIN {
		sendErrorNotAuthorize(w, fmt.Errorf("not authorize"))
		return
	}
	foundedUser, err := s.userManager.Get(userUUID)
	if err != nil {
		sendErrorNotAuthorize(w, err)
		return
	}
	sendOkResponse(w, foundedUser)
}

func (s *ServerImplementation) UpdateUser(w http.ResponseWriter, r *http.Request, userUUID api.PathUserUUID, params api.UpdateUserParams) {
}

func (s *ServerImplementation) DeleteUser(w http.ResponseWriter, r *http.Request, userUUID api.PathUserUUID) {
}
