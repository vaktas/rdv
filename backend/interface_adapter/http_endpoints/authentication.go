package httpendpoints

import (
	"net/http"

	"github.com/google/uuid"
	api "gitlab.com/vaktas/rdv/api"
	usecase "gitlab.com/vaktas/rdv/use_case"
)

func getUserAndRoleFromRequest(r *http.Request, userManager *usecase.UserManager, tokenManager *usecase.TokenManager) (user *api.User, role string, err error) {
	header, ok := r.Header["Authorization"]
	if !(ok && len(header) == 1) {
		err = &TokenBearerNotFound{}
		return
	}
	tokenId, err := uuid.Parse(header[0][7:])
	if err != nil {
		return
	}
	user, err = tokenManager.GetUser(tokenId)
	if err != nil {
		return
	}
	role, err = userManager.GetRole(user.Uuid)
	return
}
