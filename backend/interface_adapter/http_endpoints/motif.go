package httpendpoints

import (
	"fmt"
	"net/http"

	"github.com/google/uuid"
	"gitlab.com/vaktas/rdv/api"
)

func (s *ServerImplementation) SearchMotif(w http.ResponseWriter, r *http.Request, params api.SearchMotifParams) {
	if user, _, err := getUserAndRoleFromRequest(r, s.userManager, s.tokenManager); err != nil || user.Uuid == uuid.Nil {
		sendErrorNotAuthorize(w, fmt.Errorf("not logged"))
		return
	}
	motifs, err := s.motifManager.Search(params)
	if err != nil {
		sendErrorBadRequest(w, err)
		return
	}
	sendOkResponse(w, motifs)
}

func (s *ServerImplementation) AddMotif(w http.ResponseWriter, r *http.Request, params api.AddMotifParams) {
	_, role, err := getUserAndRoleFromRequest(r, s.userManager, s.tokenManager)
	if err != nil {
		sendErrorBadRequest(w, err)
		return
	}
	if err != nil || role != api.ADMIN {
		sendErrorNotAuthorize(w, err)
		return
	}
	err = s.motifManager.Create(params)
	if err != nil {
		sendErrorBadRequest(w, err)
		return
	}
}

func (s *ServerImplementation) PutMotif(w http.ResponseWriter, r *http.Request, params api.PutMotifParams) {
	_, role, err := getUserAndRoleFromRequest(r, s.userManager, s.tokenManager)
	if err != nil {
		sendErrorBadRequest(w, err)
		return
	}
	if err != nil || role != api.ADMIN {
		sendErrorNotAuthorize(w, err)
		return
	}
	err = s.motifManager.Update(params)
	if err != nil {
		sendErrorBadRequest(w, err)
		return
	}
}

func (s *ServerImplementation) DeleteMotif(w http.ResponseWriter, r *http.Request, params api.DeleteMotifParams) {
	_, role, err := getUserAndRoleFromRequest(r, s.userManager, s.tokenManager)
	if err != nil {
		sendErrorBadRequest(w, err)
		return
	}
	if err != nil || role != api.ADMIN {
		sendErrorNotAuthorize(w, err)
		return
	}
	err = s.motifManager.Delete(params)
	if err != nil {
		sendErrorBadRequest(w, err)
		return
	}
}
