package httpendpoints

import (
	"fmt"

	storage "gitlab.com/vaktas/rdv/storage"
	usecase "gitlab.com/vaktas/rdv/use_case"
)

type ServerImplementation struct {
	storageHandler     storage.StorageHandler
	motifManager       *usecase.MotifManager
	userManager        *usecase.UserManager
	tokenManager       *usecase.TokenManager
	slotsGroupManager  *usecase.SlotsGroupManager
	appointmentManager *usecase.AppointmentManager
}

func NewServer(storageHandler storage.StorageHandler) (*ServerImplementation, error) {
	if storageHandler == nil {
		return nil, fmt.Errorf("no storage set")
	}
	return &ServerImplementation{
		storageHandler:     storageHandler,
		motifManager:       &usecase.MotifManager{StorageHandler: storageHandler},
		userManager:        &usecase.UserManager{StorageHandler: storageHandler},
		tokenManager:       &usecase.TokenManager{StorageHandler: storageHandler},
		slotsGroupManager:  &usecase.SlotsGroupManager{StorageHandler: storageHandler},
		appointmentManager: &usecase.AppointmentManager{StorageHandler: storageHandler},
	}, nil
}
