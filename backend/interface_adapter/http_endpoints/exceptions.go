package httpendpoints

type TokenBearerNotFound struct{}

func (e *TokenBearerNotFound) Error() string {
	return "token not found in the header"
}
