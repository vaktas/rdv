package storage

import "fmt"

type TokenNotFound struct{}

func (e *TokenNotFound) Error() string {
	return "token not found"
}

type TokenNotExpired struct{}

func (e *TokenNotExpired) Error() string {
	return "the token has not expired yet"
}

type UserNotFound struct{}

func (e *UserNotFound) Error() string {
	return "user not found"
}

type AppointmentNotFound struct{}

func (e *AppointmentNotFound) Error() string {
	return "appointment not found"
}

type SlotsGroupNotFound struct{}

func (e *SlotsGroupNotFound) Error() string {
	return "slotsGroup not found"
}

type MotifNotFound struct{}

func (e *MotifNotFound) Error() string {
	return "motif not found"
}

type AppointmentAlreadyExists struct{}

func (e *AppointmentAlreadyExists) Error() string {
	return "appointment already exists"
}

type EmptyFieldError struct {
	EmptyField string
}

func (e *EmptyFieldError) Error() string {
	return fmt.Sprintf("field empty: %s", e.EmptyField)
}
