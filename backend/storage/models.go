package storage

import (
	"time"

	"github.com/google/uuid"
)

type StorageHandler interface {
	CreateUser(surname string, name string, tel string, password string) (*User, error)
	FindUsers(limit uint, offset uint, filter *User) ([]User, int, error)
	FindUser(userUUID uuid.UUID) (user *User, err error)
	GetUserByLoginAndPassword(login string, password string) (user User, err error)
	DeleteUser(userUUID uuid.UUID) error
	FindMotifByName(motifName string) (motif *Motif, err error)
	FindMotifs(motifName *string, used *bool) (motifs []Motif, err error)
	CreateMotif(name string) (err error)
	UpdateMotif(oldName string, newName string) (err error)
	DeleteMotif(name string) (err error)
	CreateAppointment(slotsGroup SlotsGroup, user User, at time.Time) error
	RemoveAppointment(slotsGroupUUID uuid.UUID, userUUID uuid.UUID) error
	UpdateAppointment(appointmentToUpdate Appointment) error
	CreateSlotsGroup(from time.Time, to time.Time, motifName string, quantityPerSlot uint, quantity uint, duration uint) error
	FindSlotsGroups(limit uint, offset uint, slotsGroupFilter *SlotsGroup, userFilter *User, appointmentFilter *Appointment, free bool) (slotsGroups []SlotsGroup, max int, err error)
	FindSlotsGroup(slotsGroupUUID uuid.UUID) (slotsGroup *SlotsGroup, err error)
	DeleteSlotsGroup(slotsGroupUUID uuid.UUID) error
	FindRole(ID uint) (string, error)
	CreateToken(userUUID uuid.UUID) (*Token, error)
	DeleteTokenByUserUUID(userUUID uuid.UUID) error
	FindToken(ID uuid.UUID) (token Token, err error)
}

type Appointment struct {
	At             time.Time `gorm:"not null;type:datetime"`
	Done           bool
	SlotsGroupUUID uuid.UUID `gorm:"type:char(36)"`
	UserUUID       uuid.UUID `gorm:"type:char(36)"`
}

type Template struct {
	UUID             uuid.UUID `gorm:"type:char(36);size:36;primarykey"`
	From             time.Time `gorm:"type:datetime"`
	To               time.Time `gorm:"type:datetime"`
	Motif            Motif     `gorm:"foreignKey:MotifID;constraint:OnDelete:CASCADE;"`
	MotifID          uint
	Quantity         uint // Nummber of Slots
	QuantityPerSlots uint `gorm:"default:1"` // Number of Users that can take the slot
	Duration         uint // Duration in minutes
}

type SlotsGroup struct {
	Template
	Appointments []*Appointment `gorm:"foreignKey:SlotsGroupUUID;constraint:OnDelete:CASCADE;"`
}

type Role struct {
	ID   uint   `gorm:"primarykey;autoIncrement"`
	Name string `gorm:"unique"`
}

type User struct {
	UUID         uuid.UUID      `gorm:"type:char(36);size:36;unique;primarykey"`
	Role         Role           `gorm:"foreignKey:RoleID"`
	Appointments []*Appointment `gorm:"foreignKey:UserUUID"`
	Telephone    string         `gorm:"size:10"`
	Login        string
	Password     string
	Surname      string
	Name         string
	RoleID       uint
}

type Token struct {
	ID       uuid.UUID `gorm:"type:char(36);unique;primarykey"`
	IssueAt  time.Time `gorm:"type:datetime"`
	To       time.Time `gorm:"type:datetime"`
	User     User      `gorm:"foreignKey:UserUUID"`
	UserUUID uuid.UUID `gorm:"type:char(36)"`
}

type Motif struct {
	ID   uint   `gorm:"primarykey;autoIncrement"`
	Name string `gorm:"unique"`
}
