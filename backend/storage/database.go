package storage

import (
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

const MAX_LOOP = 10

var emptyTime = time.Date(001, 01, 01, 0, 0, 0, 0, time.UTC)

type Database struct {
	DB *gorm.DB
}

func (s *Database) CreateUser(surname string, name string, tel string, password string) (*User, error) {
	surname = strings.ToLower(surname)
	name = strings.ToLower(name)
	login := fmt.Sprintf("%s_%s", surname, name)
	var user User
	tx := s.DB.Begin()
	defer tx.Commit()
	err := tx.Find(&user, "login = ?", login).Error
	if user.UUID == uuid.Nil {
		return s.createUser(login, surname, name, tel, password, tx)
	} else if err != nil {
		log.Println(err)
		return nil, err
	}

	for i := 0; i < MAX_LOOP; i++ {
		login = fmt.Sprintf("%s_%s_%d", surname, name, i)
		var user User
		err = tx.Find(&user, "login = ?", login).Error
		if user.UUID == uuid.Nil {
			return s.createUser(login, surname, name, tel, password, tx)
		} else if err != nil {
			log.Println(err)
			return nil, err
		}
	}
	return nil, nil
}
func (s *Database) createUser(login string, surname string, name string, tel string, password string, tx *gorm.DB) (*User, error) {
	result, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		log.Println(err)
	}
	user := User{
		UUID:      uuid.New(),
		Login:     login,
		Password:  string(result),
		Surname:   surname,
		Name:      name,
		Telephone: strings.Replace(tel, ".", "", -1),
		Role:      s.findUserRole(),
	}
	if err := tx.Create(&user).Error; err != nil {
		tx.Rollback()
		return nil, err
	}
	return &user, tx.Commit().Error
}
func (s *Database) FindAllUsers() ([]User, error) {
	return nil, nil
}
func (s *Database) FindUsers(limit uint, offset uint, filter *User) ([]User, int, error) {
	tx := s.DB.Begin()
	defer tx.Commit()
	if filter != nil {
		tx = s.filterUser(*filter, tx)
	}
	var users []User
	err := tx.Find(&users).Error
	if err != nil {
		return nil, 0, err
	}
	count := len(users)
	err = tx.Limit(int(limit)).Offset(int(offset)).Preload("Appointments").Find(&users).Error
	if err != nil {
		return nil, 0, err
	}
	return users, count, nil
}
func (s *Database) FindUser(UUID uuid.UUID) (user *User, err error) {
	tx := s.DB.Begin()
	defer tx.Commit()
	err = tx.Preload("Role").Preload("Appointments").Find(&user, "uuid = ?", UUID).Error
	if err != nil {
		return nil, err
	}
	return
}
func (s *Database) GetUserByLoginAndPassword(login string, password string) (user User, err error) {
	err = s.DB.Begin().Preload("Role").Find(&user, "login = ?", login).Error
	if err != nil {
		err = &UserNotFound{}
		return
	}
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err != nil {
		err = &UserNotFound{}
		return
	}
	return
}
func (s *Database) DeleteUser(UUID uuid.UUID) error {
	return nil
}
func (s *Database) FindMotifByName(motifName string) (motif *Motif, err error) {
	err = s.DB.Begin().Find(&motif, "name = ?", motifName).Commit().Error
	if err != nil {
		return nil, err
	}
	return
}
func (s *Database) FindMotifs(motifName *string, used *bool) (motifs []Motif, err error) {
	tx := s.DB.Debug().Begin()
	defer tx.Commit()
	if motifName != nil {
		tx = tx.Where("name = ?", motifName)
	}
	err = tx.Find(&motifs).Error
	if err != nil {
		return nil, err
	}
	return
}
func (s *Database) CreateMotif(name string) (err error) {
	tx := s.DB.Begin()
	defer tx.Commit()
	if name == "" {
		err = fmt.Errorf("name of a motif cannot be empty")
		return
	}
	if err = tx.Create(&Motif{Name: name}).Error; err != nil {
		tx.Rollback()
		return
	}
	return
}
func (s *Database) UpdateMotif(oldName string, newName string) (err error) {
	tx := s.DB.Begin()
	defer tx.Commit()
	if newName == "" {
		err = fmt.Errorf("name of a motif cannot be empty")
		return
	}
	if err = tx.Model(&Motif{}).Where("name = ?", oldName).Update("name", newName).Error; err != nil {
		tx.Rollback()
		return
	}
	return
}
func (s *Database) DeleteMotif(name string) (err error) {
	tx := s.DB.Begin()
	defer tx.Commit()
	motif := Motif{Name: name}
	if err = s.DB.First(&motif, "name = ?", name).Error; err != nil {
		return
	}
	if err = tx.Delete(&motif).Error; err != nil {
		tx.Rollback()
		return
	}
	return
}
func (s *Database) CreateSlotsGroup(from time.Time, to time.Time, motifName string, quantityPerSlots uint, quantity uint, duration uint) error {
	tx := s.DB.Begin()
	defer tx.Commit()
	var motif Motif
	tx.Find(&motif, "name = ?", motifName)
	if motif.ID == 0 {
		return &MotifNotFound{}
	}
	slotsGroup := SlotsGroup{
		Template: Template{
			UUID:             uuid.New(),
			From:             from,
			To:               to,
			QuantityPerSlots: quantityPerSlots,
			Quantity:         uint(quantity),
			Motif:            motif,
			Duration:         duration,
		},
	}
	if err := tx.Create(&slotsGroup).Error; err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit().Error
}
func (s *Database) DeleteSlotsGroup(slotsGroupUUID uuid.UUID) error {
	tx := s.DB.Begin()
	defer tx.Commit()
	var slotsGroup SlotsGroup
	err := tx.First(&slotsGroup, "uuid = ?", slotsGroupUUID).Error
	if err != nil {
		return err
	}
	if slotsGroup.UUID == uuid.Nil {
		return &SlotsGroupNotFound{}
	}
	if err := tx.Select("Appointments").Delete(&slotsGroup).Error; err != nil {
		tx.Rollback()
		return err
	}
	return nil
}
func (s *Database) FindSlotsGroup(slotsGroupUUID uuid.UUID) (slotsGroup *SlotsGroup, err error) {
	tx := s.DB.Begin()
	defer tx.Commit()
	err = tx.Preload("Appointments").Find(&slotsGroup, "uuid = ?", slotsGroupUUID).Error
	if err != nil {
		return nil, err
	}
	return
}
func (s *Database) CreateAppointment(slotsGroup SlotsGroup, user User, at time.Time) error {
	tx := s.DB.Begin()
	defer tx.Commit()
	appointment := Appointment{
		At:             at,
		UserUUID:       user.UUID,
		SlotsGroupUUID: slotsGroup.UUID,
	}
	if err := tx.Create(&appointment).Error; err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit().Error
}
func (s *Database) RemoveAppointment(slotsGroupUUID uuid.UUID, userUUID uuid.UUID) error {
	tx := s.DB.Begin()
	defer tx.Commit()
	var appointment Appointment
	err := tx.First(&appointment, "user_uuid = ? AND slots_group_uuid = ?", userUUID, slotsGroupUUID).Error
	if err != nil {
		return err
	}
	if appointment.UserUUID == uuid.Nil || appointment.SlotsGroupUUID == uuid.Nil {
		return &AppointmentNotFound{}
	}
	if err := tx.Where("user_uuid = ? AND slots_group_uuid = ?", userUUID, slotsGroupUUID).Delete(&appointment).Error; err != nil {
		tx.Rollback()
		return err
	}
	return nil
}
func (s *Database) UpdateAppointment(appointmentToUpdate Appointment) error {
	tx := s.DB.Begin()
	defer tx.Commit()
	var appointment Appointment
	err := tx.First(&appointment, "user_uuid = ? AND slots_group_uuid = ?", appointmentToUpdate.UserUUID, appointmentToUpdate.SlotsGroupUUID).Error
	if err != nil {
		return err
	}
	if appointment.SlotsGroupUUID == uuid.Nil || appointment.UserUUID == uuid.Nil {
		return &AppointmentNotFound{}
	}
	if appointmentToUpdate.Done != appointment.Done {
		if err := tx.Model(&appointment).Where("user_uuid = ? AND slots_group_uuid = ?", appointmentToUpdate.UserUUID, appointmentToUpdate.SlotsGroupUUID).Updates(Appointment{Done: appointmentToUpdate.Done}).Error; err != nil {
			return err
		}
	}
	return nil
}
func (s *Database) FindSlotsGroups(limit uint, offset uint, slotsGroupFilter *SlotsGroup, userFilter *User, appointmentFilter *Appointment, free bool) (slotsGroups []SlotsGroup, max int, err error) {
	tx := s.DB.Begin()
	defer tx.Commit()
	if free {
		return s.findFreeAppointments(limit, offset, slotsGroupFilter, tx)
	}
	return s.findAppointmentsFromQuery(limit, offset, slotsGroupFilter, userFilter, appointmentFilter, tx)
}
func (s *Database) findFreeAppointments(limit uint, offset uint, filter *SlotsGroup, tx *gorm.DB) (slotsGroups []SlotsGroup, max int, err error) {
	subQuery := tx.Select("slots_groups.*, slots_group_uuid").Table("slots_groups").Joins("LEFT JOIN appointments ON slots_group_uuid = uuid").Having("(quantity * quantity_per_slots > COUNT(*) OR quantity * quantity_per_slots = 1 AND appointments.slots_group_uuid IS NULL)").Group("uuid")
	query := tx.Table("(?) as slots_groups", subQuery)
	return s.findAppointmentsFromQuery(limit, offset, filter, nil, nil, query)
}
func (s Database) findAppointmentsFromQuery(limit uint, offset uint, slotsGroupFilter *SlotsGroup, userFilter *User, appointmentFilter *Appointment, tx *gorm.DB) (slotsGroups []SlotsGroup, max int, err error) {
	if slotsGroupFilter != nil {
		tx = s.filterSlotsGroup(*slotsGroupFilter, tx)
	}
	if userFilter != nil && userFilter.UUID != uuid.Nil {
		tx.Joins("JOIN appointments ON slots_group_uuid = uuid").Where("user_uuid = ?", userFilter.UUID)
	}
	err = tx.Find(&slotsGroups).Error
	if err != nil {
		slotsGroups = []SlotsGroup{}
		return
	}
	max = len(slotsGroups)
	err = tx.Limit(int(limit)).Offset(int(offset)).Preload("Motif").Preload("Appointments", func(db *gorm.DB) *gorm.DB {
		return db.Order("appointments.at ASC")
	}).Order("slots_groups.from ASC").Find(&slotsGroups).Error
	if err != nil {
		slotsGroups = []SlotsGroup{}
		return
	}
	return
}
func (s *Database) FindRole(ID uint) (string, error) {
	var role Role
	tx := s.DB.Begin()
	defer tx.Commit()
	err := tx.Find(&role, ID).Error
	if err != nil {
		return "", err
	}
	return role.Name, nil
}
func (s *Database) CreateToken(userUUID uuid.UUID) (*Token, error) {
	now := time.Now()
	var existingToken Token
	tx := s.DB.Begin()
	defer tx.Commit()
	tx.Where(&Token{UserUUID: userUUID}).Find(&existingToken)
	if existingToken.To.Unix() > now.Unix() {
		return &existingToken, &TokenNotExpired{}
	} else {
		tx.Delete(&existingToken, "id = ?", existingToken.ID)
	}

	lifetime := now.Add(time.Minute * 30)
	token := &Token{
		ID:       uuid.New(),
		UserUUID: userUUID,
		IssueAt:  now,
		To:       lifetime,
	}
	if err := tx.Create(token).Error; err != nil {
		tx.Rollback()
		return nil, err
	}
	return token, tx.Commit().Error
}
func (s *Database) DeleteTokenByUserUUID(userUUID uuid.UUID) error {
	tx := s.DB.Begin()
	defer tx.Commit()
	var token Token
	err := tx.First(&token, "user_uuid = ?", userUUID).Error
	if err != nil {
		return err
	}
	if token.ID == uuid.Nil {
		return &TokenNotFound{}
	}
	if err := tx.Delete(&token, "id = ?", token.ID).Error; err != nil {
		return err
	}
	return nil
}
func (s *Database) FindToken(ID uuid.UUID) (token Token, err error) {
	tx := s.DB.Begin()
	defer tx.Commit()
	err = tx.Preload("User").Find(&token, "id = ?", ID).Error
	if token.ID == uuid.Nil {
		err = &TokenNotFound{}
		return
	}
	return
}
func NewDatabase() (*Database, error) {
	user := os.Getenv("DB_USER")
	if user == "" {
		return nil, fmt.Errorf("no DB_USER in the environment")
	}
	password := os.Getenv("DB_PASSWORD")
	if password == "" {
		return nil, fmt.Errorf("no DB_PASSWORD in the environment")
	}
	name := os.Getenv("DB_NAME")
	if name == "" {
		return nil, fmt.Errorf("no DB_NAME in the environment")
	}
	host := os.Getenv("DB_HOST")
	if host == "" {
		return nil, fmt.Errorf("no DB_NAME in the environment")
	}
	port := os.Getenv("DB_PORT")
	if port == "" {
		return nil, fmt.Errorf("no DB_PORT in the environment")
	}
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", user, password, host, port, name)
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	db.AutoMigrate(&Role{}, &User{}, &Motif{}, &Template{}, &SlotsGroup{}, &Appointment{}, &Token{})

	sqlDB, _ := db.DB()

	sqlDB.SetMaxIdleConns(10)
	sqlDB.SetMaxOpenConns(100)
	sqlDB.SetConnMaxLifetime(time.Second * 10)

	return &Database{
		DB: db,
	}, nil
}

func (s *Database) findUserRole() (role Role) {
	s.DB.Find(&role, "name = ?", "user")
	return
}
func (s *Database) filterSlotsGroup(filter SlotsGroup, tx *gorm.DB) *gorm.DB {
	if filter.UUID != uuid.Nil {
		tx = tx.Where("slots_groups.uuid = ?", filter.UUID)
	}
	if filter.From != emptyTime {
		tx = tx.Where("slots_groups.from >= ?", filter.From)
	}
	if filter.To != emptyTime {
		tx = tx.Where("slots_groups.to <= ?", filter.To.Add(time.Hour*24))
	}
	if filter.Motif.Name != "" {
		tx = tx.Where("slots_groups.motif_id = ?", filter.Motif.ID)
	}
	return tx
}
func (s *Database) filterUser(filter User, tx *gorm.DB) *gorm.DB {
	if filter.Login != "" {
		tx = tx.Where("users.login", filter.Login)
	}
	if filter.Name != "" {
		tx = tx.Where("users.name", filter.Name)
	}
	if filter.Surname != "" {
		tx = tx.Where("users.surname", filter.Surname)
	}
	if filter.UUID != uuid.Nil {
		tx = tx.Where("users.uuid", filter.UUID)
	}
	if filter.Telephone != "" {
		tx = tx.Where("users.telephone", strings.Replace(filter.Telephone, ".", "", -1))
	}
	return tx
}
