package storage

import (
	"time"

	"gitlab.com/vaktas/rdv/api"
)

func SlotsGroupToSlotsGroupEntity(slotsGroup SlotsGroup, loggedUser api.User, roleUser string, needSpecificUser bool, free bool) *api.SlotsGroup {
	if roleUser != api.ADMIN && !free && needSpecificUser && len(slotsGroup.Appointments) == 0 {
		return nil
	}
	appointments := []api.Appointment{}
	for _, a := range slotsGroup.Appointments {
		if roleUser != api.ADMIN && loggedUser.Uuid == a.UserUUID && free {
			return nil
		}
		if roleUser == api.ADMIN || loggedUser.Uuid == a.UserUUID {
			appointments = append(appointments, api.Appointment{
				From: a.At,
				To:   a.At.Add(time.Duration(slotsGroup.Duration * uint(time.Minute))),
				User: &a.UserUUID,
				Done: a.Done,
			})
		} else {
			appointments = append(appointments, api.Appointment{
				From: a.At,
				To:   a.At.Add(time.Duration(slotsGroup.Duration * uint(time.Minute))),
				Done: a.Done,
			})
		}
	}
	return &api.SlotsGroup{
		Uuid:            slotsGroup.UUID,
		Motif:           slotsGroup.Motif.Name,
		From:            slotsGroup.From,
		To:              slotsGroup.To,
		Quantity:        int32(slotsGroup.Quantity),
		QuantityPerSlot: int32(slotsGroup.QuantityPerSlots),
		Duration:        int32(slotsGroup.Duration),
		Appointments:    appointments,
	}
}
func UserToUserEntity(user *User) *api.User {
	return &api.User{
		Uuid:    user.UUID,
		Login:   user.Login,
		Name:    user.Name,
		Surname: user.Surname,
		Tel:     user.Telephone,
	}
}
func MotifToMotifEntity(motif Motif) *api.Motif {
	motifEntity := api.Motif(motif.Name)
	return &motifEntity
}
